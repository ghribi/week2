import java.util.Random
import org.apache.log4j.Logger
import org.apache.log4j.Level
import scala.tools.nsc.io.File

import org.apache.spark.sql.SparkSession
import org.apache.spark.rdd._
import org.apache.spark.mllib.recommendation.{ALS, Rating, MatrixFactorizationModel}

object MFRecommender {
  def main(args: Array[String]) {
    Logger.getLogger("org").setLevel(Level.ERROR)
    Logger.getLogger("akka").setLevel(Level.ERROR)

    val spark = SparkSession.builder
                    .appName("MF Recommender").getOrCreate()
    val (ratings, movies) = loadData(spark)

    val numRatings = ratings.count
    val numUsers = ratings.map(_._2.user).distinct.count
    val numMovies = ratings.map(_._2.product).distinct.count
    println(s"Got $numRatings ratings from $numUsers users on " + 
            s"$numMovies movies.")

    val (training, validation, test) = splitRatings(ratings)
    val numTraining = training.count
    val numValidation = validation.count

    val (bestModel, bestRank, bestLambda, bestNumIter) =
            tuneParameters(training, validation, numValidation)

    val movieFeatures = bestModel.get.productFeatures

    val genomeResult = getProcessed()    
    val preProcessed = getPreprocessed()
    val intersectNow =
            for (m <- genomeResult
                 if !preProcessed.keySet.contains(m._1))
            yield {
                val knn = knearest(m._1, 100, movieFeatures)
                logAppend(m._1, knn) 
                knn.toSet.intersect(m._2.toSet).size
            }

    val intersectPrev =
        for ((k, v1) <- genomeResult;
             (`k`, v2) <- preProcessed)
        yield v1.toSet.intersect(v2.toSet).size

    val intersects = intersectNow.toList ::: intersectPrev.toList
    val total = intersects.size
    println(s"Similarity between KNN genome and MF ($bestRank features): " + 
                (intersects.sum.toFloat / total).toString + s"% for $total movies")

    spark.stop()
  }

//------------------------------------------------------------------------------
    def knearest(movieId: Int, k: Int,
                 features: RDD[(Int, Array[Double])]) = {
        def summation(p: Array[Double], q: Array[Double]) = {
            p.zip(q).map{case (a, b) => (b - a) * (b - a)}.sum
        }       
            
        try {
            val p = features.lookup(movieId).head
            val distances =
                    for (movie <- features;
                         q = movie._2;
                         distance = math.sqrt(summation(p, q))
                         if (movie._1 != movieId)
                    ) yield (movie._1, distance)
            distances.sortBy(_._2).take(k).map(_._1)
        } catch {   
            case _ : Throwable => Array[Int]()
        }
   } 

//------------------------------------------------------------------------------
    val ppfname = "knnGenome-ml-25m.txt"
    def getProcessed() = {
        File(ppfname).createFile().lines()
            .map { l => 
                val values = l.split(",")
                (values.head.toInt, values.tail.map(_.toInt))
            }.toMap
    } 

//------------------------------------------------------------------------------
    val filename = "knnMF-ml-25m.txt"
    def getPreprocessed() = {
        File(filename).createFile().lines()
            .map { l => 
                val values = l.split(",")
                (values.head.toInt, values.tail.map(_.toInt))
            }.toMap
    } 

    def logAppend(id: Int, knn: Array[Int]) = {
        File(filename).appendAll(id.toString + "," + knn.mkString(",") + "\n")
    }

//------------------------------------------------------------------------------
  def loadData(spark: SparkSession): (RDD[(Long, Rating)], Map[Int, String]) = {
    import spark.implicits._
    // load ratings and movie titles
    val movieLensHomeDir = "/cs449/movielens/ml-25m"

    val splittoken = ","
    val ratings = spark.read.options(Map("header" -> "true"))
                        .csv(movieLensHomeDir + "/ratings.csv").rdd
                        .map(r => (r.getString(3).toLong % 10,
                                   Rating(r.getString(0).toInt,
                                          r.getString(1).toInt,
                                          r.getString(2).toDouble))
                        )
    val movies =  spark.read.options(Map("header" -> "true"))
                       .csv(movieLensHomeDir + "/movies.csv")
                       .map(r => (r.getString(0).toInt, r.getString(1)))
                       .collect.toMap
    (ratings, movies)
  }

//------------------------------------------------------------------------------
  def tuneParameters(training: RDD[Rating], validation: RDD[Rating], 
                     validationCount: Long) = {
    // train models and evaluate them on the validation set
    val ranks = List(36)
    val lambdas = List(0.1)
    val numIters = List(20)
    var bestModel: Option[MatrixFactorizationModel] = None
    var bestValidationRmse = Double.MaxValue
    var bestRank = 0
    var bestLambda = -1.0
    var bestNumIter = -1
    for (rank <- ranks; lambda <- lambdas; numIter <- numIters) {
      val model = ALS.train(training, rank, numIter, lambda)
      val validationRmse = computeRmse(model, validation, validationCount)
      println(s"RMSE (validation) = $validationRmse for the " +
              s"model trained with rank = $rank lambda = $lambda" +
              s" and numIter = $numIter.")
      if (validationRmse < bestValidationRmse) {
        bestModel = Some(model)
        bestValidationRmse = validationRmse
        bestRank = rank
        bestLambda = lambda
        bestNumIter = numIter
      }
    }
    (bestModel, bestRank, bestLambda, bestNumIter)
  }

//------------------------------------------------------------------------------
  def splitRatings(ratings: RDD[(Long,Rating)]) = {
    // split ratings into train (60%), validation (20%), and test (20%) based on the 
    // last digit of the timestamp, add myRatings to train, and cache them
    val numPartitions = 20
    val training = ratings.filter(x => x._1 < 6)
                          .values
                          .repartition(numPartitions)
                          .persist
    val validation = ratings.filter(x => x._1 >= 6 && x._1 < 8)
                            .values
                            .repartition(numPartitions)
                            .persist
    val test = ratings.filter(x => x._1 >= 8).values.persist
    (training, validation, test)
  }

//------------------------------------------------------------------------------
  def computeRmse(model: MatrixFactorizationModel, data: RDD[Rating], n: Long) = {
    val predictions: RDD[Rating] =
                  model.predict(data.map(x => (x.user, x.product)))
    val predictionsAndRatings = 
              predictions.map(x => ((x.user, x.product), x.rating))
                         .join(data.map(x => ((x.user, x.product), x.rating)))
                         .values
    math.sqrt(predictionsAndRatings
                .map(x => (x._1 - x._2) * (x._1 - x._2))
                .reduce(_ + _)
              / n)
  }

//------------------------------------------------------------------------------
}
