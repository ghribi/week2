import scala.io.Source

object Genome {
    def parseGenome(lines: Iterator[String]):  Map[Int, List[(Int, Double)]] = {
        val parsed =
            for (line <- lines;
                 Array(movie, tag, relevance) = line.split(",")
            ) yield (movie.toInt, tag.toInt, relevance.toDouble)
        val genomes = parsed.toList
        genomes.groupBy(t => t._1)
               .mapValues(l => l.map(triplet => (triplet._2, triplet._3)))
    }

    def parseMovies(lines: Iterator[String]) = {
        val parsed =
            for (line <- lines;
                 splitted = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1)
            ) yield (splitted(0).toInt, splitted(1).replaceAll("^\"|\"$", ""))
        parsed.toMap
    }

    def knearest(movieId: Int, k: Int, 
                 genome: Map[Int, List[(Int, Double)]]) = {
        def summation(p: List[(Int, Double)], q: List[(Int, Double)]) = {
            val squareOfDiff =
                for((tag, relevance1) <- p;
                    (`tag`, relevance2) <- q;
                    diff = relevance2 - relevance1
                ) yield diff * diff 
            squareOfDiff.sum
        }

        val p = genome(movieId)
        val distances =
            for (movie <- genome;
                 q = movie._2;
                 distance = math.sqrt(summation(p, q))
                 if (movie._1 != movieId)
            ) yield (movie._1, distance)
        distances.toList.sortBy(_._2).take(k).map(_._1)
    }

    def nicePrint(movieId: Int, k: Int, movies: Map[Int, String],
                  data: Map[Int, List[(Int, Double)]]) = {
        val knn = knearest(movieId, k, data)
        println(s"The $k nearest movies from " + movies(movieId) + " are: ")
        knn.map(movies(_)) foreach println
    }

    def main(args: Array[String]) {
       val genomeFile = "ml-25m/genome-scores.csv"
       val moviesFile = "ml-25m/movies.csv"
       val genByMovie =
            parseGenome(Source.fromFile(genomeFile).getLines
                              .drop(1) // drop the header
                              .take(2e+6.toInt)) // take 2M lines
       val movieNames = parseMovies(Source.fromFile(moviesFile).getLines
                              .drop(1)) // drop the header
            
       nicePrint(260, 10, movieNames, genByMovie)
    }
}
